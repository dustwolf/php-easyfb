<?php

//This is a port of a QBasic program I made as a kid
//Yes I didn't care much about good practices
//Given the filename and the end result, I gather I didn't achieve what I set out to do

require_once "inc/fbHTML.php";
$screen = new fbHTML("Crystal", "1mm");

$screen->mode(13);

$b = 0; $g = 0; $r = 0; $bb = 0; $gg = 0; $rr = 0;

define("Q", 1.5); define("MA", 40);

require_once "inc/easyfb.php";
$fb = new easyfb([320, 200]);

$bb = 1;
$screen->window();
foreach(range(1,255) as $s) {
  $b = $b + $bb;
  $g = $g + $gg;
  $r = $r + $rr;
  
  if($b == 30) { $gg = 1; }
  if($b == 63) { $bb = 0; }
  if($g == 63) { $gg = 0; }
  if($g == 20) { $rr = 1; }
  if($r == 63) { $rr = 0; $gg = 0; $bb = 0; }

  //$screen->palette($s, 65536 * $b + 256 * $g + $r);
  $screen->palette($s, 65536 * $r + 256 * $b + $g);
  $fb->line([0,0], [1, 200], $s, "BF");

}

$fb->cls();

$fb->line([0,0], [MA,0], 255, "BF");

foreach(range(0, 200) as $b) {
  foreach(range(0, MA) as $a) {
  
    $s = $fb->point([$a, $b]);
    if($a == MA && $s == 0) { goto end; }
    
    $n = 1;
    foreach(range(1, Q * (MA / 10)) as $w) {
      $s = $s + $fb->point([$a + (int) $w, $b]) + $fb->point([$a - (int) $w, $b]);
      $n = $n + 2;
    }
    
    foreach(range(0, 320, MA * 1.2) as $i) {
      $fb->pset([$a + ((int) $i), $b + 1], (int) $s / $n);
    }    
    $s = 0;
  }
}

end:
$screen->render($fb->frameBuffer);
