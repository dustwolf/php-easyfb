<?php

class html {

 public $jQuery;
 public $chosen;
 public $bootstrap;
 public $css;
 public $inlinecss;
 public $font;
 public $js;

 private $jscache;

 function __construct($naslov = "", $opcije = array()) {
  $this->jQuery = isset($opcije["jQuery"]);
  $this->chosen = isset($opcije["chosen"]);
  $this->bootstrap = isset($opcije["bootstrap"]);
  if(isset($opcije["css"])) {
   $this->css = $opcije["css"];
  } else {
   $this->css = "";
  }
  if(isset($opcije["inlinecss"])) {
   $this->inlinecss = $opcije["inlinecss"];
  } else {
   $this->inlinecss = "";
  }
  if(isset($opcije["font"])) {
   $this->font = $opcije["font"];
  } else {
   $this->font = "";
  }
  if(isset($opcije["js"])) {
   $this->js = $opcije["js"];
  } else {
   $this->js = "";
  }
  if($this->chosen || $this->bootstrap) {
   $this->jQuery = True;
  }

  $this->jscache = array();
 
?>
<!DOCTYPE html>
<html>
 <head>
  <meta charset="UTF-8">
  <title><?php echo $naslov; ?></title>
  <?php if($this->jQuery) { ?>
   <script src="css/jquery-1.11.3.min.js"></script>
  <?php } ?>
  <?php if($this->js != "") { ?>
   <script src="css/<?php echo $this->js; ?>"></script>
  <?php } ?>
  <?php if($this->bootstrap) { ?>
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/bootstrap-theme.min.css">
  <?php } ?>
  <?php if($this->chosen) { ?>
   <link rel="stylesheet" href="css/chosen.min.css">
  <?php } ?>
  <?php if($this->css != "") { ?>
   <link rel="stylesheet" href="css/<?php echo $this->css; ?>">
  <?php } ?>
  <?php if($this->inlinecss != "") { ?>
   <style>
    <?php echo $this->inlinecss; ?>">
   </style>
  <?php } ?>
  <?php if($this->font != "") { ?>
   <link href="css/<?php echo $this->font; ?>" rel="stylesheet" type="text/css">
  <?php } ?>
 </head>
 <body>
<?php
 }

 function test() {
  ?>WORKS!<?php
 }

 //add HTML / PHP section from file, note: only $parameters and $out pass in/out of this context, but you can use $this
 function add($page = False, $parameters = False) { 
  $out = False;
  if($page !== False) {
   $path = __DIR__."/".$page.".php";
   if(file_exists($path)) { //no stringent path checking because this isn't meant to process user input
    $out = False;
    include $path;
   }
  }
  return $out;
 }

 function addJS ($js = "") {
  $this->jscache[] = $js;
 }

 function hereJS() {
  echo implode("", $this->jscache);
  $this->jscache = array();
 }

 function __destruct() {
?>
 <?php if($this->bootstrap) { ?>
  <script src="css/bootstrap.min.js"></script>
 <?php } ?>
 <?php if($this->chosen) { ?>
  <script src="css/chosen.jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript">
   var config = {
    'select' : {search_contains: true, width: '100%'},
   }
   for (var selector in config) {
    $(selector).chosen(config[selector]);
   }
  </script>
 <?php } ?> 
 <?php 
  if($this->jscache != array()) {
   $this->hereJS(); 
  }
 ?>
 </body>
</html>
<?php
 }

}
